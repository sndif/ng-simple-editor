angular
  .module 'ngSimpleEditor', []
  .directive 'ngSimpleEditor', [
    '$timeout'
    ( $timeout ) ->
      {
        restrict : 'E'
        replace : true
        template : '<TEMPLATE>'
        require : [ 'ngModel' ]
        scope :
          ngModel : '='
        link : ( $scope, $elem, attrs, ctrls ) ->

          document.execCommand 'defaultParagraphSeparator', false, 'p'

          $scope.$content = $elem.find( '.content' )

          if $scope.ngModel
            $scope.$content.html $scope.ngModel

          $scope.updateModel = ->
            $timeout ->
              $scope.$apply ->
                $scope.ngModel = $scope.$content.html()

          $scope.$content.bind 'keypress keydown keyup focus blur', ( e ) ->
            $scope.updateModel()

      }
  ]

  .directive 'edAction', ->
    {
      restrict : 'A'
      scope :
        edAction : '@'
      link : ( $scope, $elem, attar, ctrls ) ->
        $elem.bind 'click', ( e ) ->
          e.preventDefault()
          switch $scope.edAction
            when 'h1', 'h2', 'h3', 'p'
              document.execCommand 'formatBlock', false, $scope.edAction
            else
              document.execCommand $scope.edAction, false, null

          $scope.$parent.updateModel()
    }

